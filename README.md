## CHEATSHEET for Linux
This is a simple program that enables one to quickly jot down important notes and save on file

## USAGE 

                           USAGE                                
- cheat -e  filename       ->      This creates a new file        
- cheat -r  filename       ->      This reads from a file         
- cheat filename           ->      This reads from a file         
- cheat -l                 ->      Prints all files in .cheat dir 
- cheat -h                 ->      Prints usage 

## Prerequisites

- Python3

## Installation

- You need to download cheat.py file
- pip3 install pyinstaller
- pyinstaller cheat.py --onefile
- cd into `dist` folder 
- Move binary into /usr/bin


