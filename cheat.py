#!/usr/bin/env python3
import sys, subprocess, os

class cheat():
    def __init__(self) -> None:
        args = sys.argv
        self.home = os.path.expanduser('~')
        self.target_dir = os.path.join(self.home, ".cheat")
        if not os.path.exists(self.target_dir):
            os.mkdir(self.target_dir)
        if len(args) > 1 and args[1] == '-e':
            if not os.path.exists(self.target_dir):
                os.mkdir(self.target_dir)
                self.create_file(args[2])
            else:
                os.chdir(self.target_dir)
                self.create_file(args[2])
        elif len(args) > 1 and args[1] == '-l':
            os.chdir(self.target_dir)
            lst = os.listdir(self.target_dir)
            for item in lst:
                print(item)
        elif len(args) > 1 and args[1] == '-r':
            os.chdir(self.target_dir)
            fname = f"{args[2]}"
            if os.path.exists(self.target_dir+'/'+fname):
                self.read_file(args[2])
            else:
                print(f"No file named: {fname}")
        elif len(args) == 2 and not args[1] == "-h":
            os.chdir(self.target_dir)
            fname = f"{args[1]}"
            if os.path.exists(self.target_dir+'/'+fname):
                self.read_file(args[1])
            else:
                print(f"No file named: {fname}")
        elif len(args) > 1 and args[1] == "-h":
            self.usage()
        else:
            self.usage()

    def create_file(self, file_name):
        subprocess.call(["vim", f"{file_name}"])
    def read_file(self, file_name):
        subprocess.call(["vim", f"{file_name}"])
    def usage(self):
        print("                           USAGE                                ")
        print("Cheat -e  filename       ->      This creates a new file        ")
        print("Cheat -r  filename       ->      This reads from a file         ")
        print("Cheat filename           ->      This reads from a file         ")
        print("Cheat -l                 ->      Prints all files in .cheat dir ")
        print("Cheat -h                 ->      Prints usage                   ")

    

if __name__ == '__main__':
    n = cheat()
